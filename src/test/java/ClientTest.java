import org.ekwateur.ParticularClient;
import org.ekwateur.ProClient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClientTest {

    @Test
    public void testFacturerClientPro() {
        ProClient clientPro1 = new ProClient("12345678901234", "Société Exemple 1", 1500000);
        ProClient clientPro2 = new ProClient("23456789012345", "Société Exemple 2", 500000);

        double consoElectricite = 2000;
        double consoGaz = 1000;

        double facturePro1 = clientPro1.facturer(consoElectricite, consoGaz);
        double facturePro2 = clientPro2.facturer(consoElectricite, consoGaz);

        assertEquals(consoElectricite * 0.114 + consoGaz * 0.111, facturePro1, 0.001);
        assertEquals(consoElectricite * 0.118 + consoGaz * 0.113, facturePro2, 0.001);
    }


    @Test
    public void testFacturerClientParticulier() {
        ParticularClient clientParticulier1 = new ParticularClient("M.", "Doe", "John");
        ParticularClient clientParticulier2 = new ParticularClient("Mme.", "Doe", "Jane");

        double consoElectricite = 2000;
        double consoGaz = 1000;

        double factureParticulier1 = clientParticulier1.facturer(consoElectricite, consoGaz);
        double factureParticulier2 = clientParticulier2.facturer(consoElectricite, consoGaz);

        assertEquals(2000 * 0.121 + 1000 * 0.115, factureParticulier1, 0.001);
        assertEquals(2000 * 0.121 + 1000 * 0.115, factureParticulier2, 0.001);
    }
}
