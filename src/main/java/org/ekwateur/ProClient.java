package org.ekwateur;

public class ProClient extends Client {
	private String siret;
	private String socialReason;
	private double ca;

	public ProClient(String siret, String socialReason, double ca) {
		super();
		this.siret = siret;
		this.socialReason = socialReason;
		this.ca = ca;
	}

	@Override
	public double invoice(double consoElectricity, double consoGaz) {
		double priceElectricity;
		double priceGaz;

		if (ca > 1000000) {
			priceElectricity = 0.114;
			priceGaz = 0.111;
		} else {
			priceElectricity = 0.118;
			priceGaz = 0.113;
		}

		return consoElectricity * priceElectricity + consoGaz * priceGaz;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public String getSocialReason() {
		return socialReason;
	}

	public void setSocialReason(String socialReason) {
		this.socialReason = socialReason;
	}

	public double getCa() {
		return ca;
	}

	public void setCa(double ca) {
		this.ca = ca;
	}

}
