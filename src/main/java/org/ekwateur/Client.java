package org.ekwateur;

import java.util.Random;

public abstract class Client {
	private String refClient;

	public Client() {
		this.refClient = generateRefClient();
	}

	// Chaque énergie est facturée au kWh
	public abstract double invoice(double electricityConso, double gazconso);

	// Reference Client (EKW + 8 caractères numériques)
	public String generateRefClient() {
		Random random = new Random();
		StringBuilder reference = new StringBuilder("EKW");
		for (int i = 0; i < 8; i++) {
			reference.append(random.nextInt(10));
		}
		return reference.toString();
	}

	public String getRefClient() {
		return refClient;
	}

	public void setRefClient(String refClient) {
		this.refClient = refClient;
	}

}
