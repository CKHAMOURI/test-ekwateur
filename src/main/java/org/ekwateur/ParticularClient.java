package org.ekwateur;

public class ParticularClient extends Client {
	private String civilite;
	private String nom;
	private String prenom;

	public ParticularClient(String civilite, String nom, String prenom) {
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
	}

	@Override
	public double invoice(double consoElectricity, double consoGaz) {
		double priceElectricite = 0.121;
		double priceGaz = 0.115;

		return consoElectricity * priceElectricite + consoGaz * priceGaz;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}