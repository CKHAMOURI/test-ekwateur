package org.ekwateur;

public class Main {
	public static void main(String[] args) {
		ProClient clientPro1 = new ProClient("12345678901234", "Société Exemple 1", 1500000);
		ProClient clientPro2 = new ProClient("23456789012345", "Société Exemple 2", 500000);
		ParticularClient particularClient1 = new ParticularClient("M.", "Doe", "John");
		ParticularClient particularClient2 = new ParticularClient("Mme.", "Doe", "Jane");

		double consoElectricite = 2000;
		double consoGaz = 1000;
		System.out.println(clientPro1.generateRefClient());
		System.out.println(clientPro2.generateRefClient());
		System.out.println(particularClient1.generateRefClient());
		System.out.println(particularClient2.generateRefClient());
		System.out.println("Facture Client Pro 1 (CA > 1 000 000 €) : " + clientPro1.invoice(consoElectricite, consoGaz) + " €");
		System.out.println("Facture Client Pro 2 (CA < 1 000 000 €) : " + clientPro2.invoice(consoElectricite, consoGaz) + " €");
		System.out.println("Facture Client Particulier 1 : " + particularClient1.invoice(consoElectricite, consoGaz) + " €");
		System.out.println("Facture Client Particulier 2 : " + particularClient2.invoice(consoElectricite, consoGaz) + " €");
	}
}
